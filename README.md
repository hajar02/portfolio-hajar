

git@gitlab.com:hajar02/portfolio-hajar.git

Wireframe + maquette

https://www.figma.com/file/0jFzNmU4QAtWdmA0DI4reg/Untitled?node-id=0%3A1&t=787OKp8HGJhvm5xV-1



Mon portfolio est composé d'un header, où à l'intérieur on retrouve une navbar, 5 sections (présentation, projets, compétences, formations et langues) et à la fin un footer où se trouvent mes différents contacts. J'ai choisi de mettre mon site sur un fond sombre afin de mettre en valeur le contenu.

Dans la première section, j'ai inséré ma courte présentation avec un bouton qui mène au lien de mon CV créé avec canva.

Dans la deuxième section j'ai placé certains de mes projets (cette section est encore à terminer), en cliquant sur les moodboards de mes projets la présentation du projet apparaîtra dans une autre fenêtre.

Dans la troisième section, il y a un curseur automatique de compétences pratiques.

Dans la quatrième section, j'ai inséré mon programme d'études, et en cliquant sur chaque étape, vous pouvez avoir des informations supplémentaires.

Dans la cinquième section, j'ai entré mon niveau dans les différentes langues.

